# test electron vuejs app for emergen

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[8d4ed60](https://github.com/SimulatedGREG/electron-vue/tree/8d4ed607d65300381a8f47d97923eb07832b1a9a) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).

## Set developer environment

```
# Install vue-cli and scaffold boilerplate
npm install -g vue-cli

# Install dependencies and run the app
yarn # or npm install
yarn run dev # or npm run dev
```

## Build

```
yarn run build
```
